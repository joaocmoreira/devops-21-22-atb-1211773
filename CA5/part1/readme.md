# CA5 - CI/CD Pipelines with Jenkins - part 1



<br>
 

## Part 2.1

## Install Jenkins

Jenkins can be installed through different ways. I choosed to download the war file and running it.

Running directly the War file
```
java -jar jenkins.war
```

![Jenkins Start](images/CA5-part1_jenkins_startup.png)

After installation we need to create a admin user to access the Jenkins dashboard

<br>

---

<br>


## Part 2.2

## Jenkins credentials ID

Dashboard > User > Credentials

To use external connections we need to create extra credentials. We need to do it to be able to use Git. As the repository was publlic we don't need to set it in the checkout setup.

---

<br>


## Part 2.3

## New pipeline and Checkout - Setup


Pipeline Job 
Configure > Pipeline


<em>Checkout - Via Script</em><br>

```Jenkinsfile

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                 git 'https://bitbucket.org/joaocmoreira/devops-21-22-atb-1211773'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir ("CA2/Part1/gradle_basic_demo") {
                bat 'gradlew assemble' }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir ("CA2/Part1/gradle_basic_demo") {
                bat 'gradlew test' }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir ("CA2/Part1/gradle_basic_demo") {
                archiveArtifacts 'build/distributions/*'}
            }
        }
    }
}
```

<br>

![Jenkins](images/CA5-part1_jenkins.png)