# CA5 - CI/CD Pipelines with Jenkins and docker



## Part 2.1

## Install Jenkins plugins

- Jenkins Home page > pluginManager

> Javadoc Plugin - This plugin adds Javadoc support to Jenkins.

> HTML Publisher plugin - This plugin publishes HTML reports.

> JUnit - Allows JUnit-format test results to be published.


---

<br>



## Part 2.2

## Pipeline Script


```Jenkinsfile
pipeline {
 agent any
 

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://JoaoCMoreira@bitbucket.org/joaocmoreira/devops-21-22-atb-1211773'
                }
 }
 stage('Assemble'){
            steps {
                echo 'Assembling...'
                dir ("CA2/part2/react-and-spring-data-rest-basic") {
                    script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew assemble'}
                    else
                        bat './gradlew assemble'
                    }
                }
            }
        }

        stage('Test') {
            steps {
                echo 'testing...'
                dir ("CA2/part2/react-and-spring-data-rest-basic"){
                     script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew test'
                        junit 'build/test-results/test/*.xml'}
                    else
                    {
                        bat './gradlew test'
                        junit 'build/test-results/test/*.xml'}
                    }
                }
            }
        }
 stage ('Javadoc'){
     steps{
         dir("CA2/Part2/react-and-spring-data-rest-basic"){
         echo 'Publishing...'
         bat './gradlew javadoc'
         publishHTML([allowMissing: false,
          alwaysLinkToLastBuild: true,
           keepAll: true, reportDir: 'build/docs/javadoc',
            reportFiles: 'index.html', reportName: 'HTML Report',
             reportTitles: ''])

         }
     }
 }
    stage('Archiving') {
        steps {
            echo 'Archiving...'
            dir ("CA2/Part2/react-and-spring-data-rest-basic"){
            archiveArtifacts 'build/libs/'
            }
         }
    }
    stage ('Docker Image') {
        steps {
            echo 'Publishing...'
            dir("CA2/Part2/react-and-spring-data-rest-basic") {

script {
   docker.withRegistry('https://registry.hub.docker.com/', 'docker_credentials') {

        def customImage = docker.build("joaocmoreira/ca5-part2:${env.BUILD_ID}")

                customImage.push()
                        }
        }   
      }
            }
        }
    }
}
```


```Dockerfile
FROM tomcat:8-jdk8

RUN apt-get update -y

RUN apt-get install sudo nano git nodejs npm -f -y


RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://JoaoCMoreira@bitbucket.org/joaocmoreira/devops-21-22-atb-1211773.git

WORKDIR /tmp/build/devops-21-22-atb-1211773/CA2/Part2/react-and-spring-data-rest-basic

  ADD ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8085

```


---

<br>



## Part 2.3

## Pipeline Stage

![Jenkins](images/CA5-part2_jenkins.png)

![DocherHub](images/CA5-part2_dockerhub.png)
