package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.InputMismatchException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class EmployeeTest {

    @Test
    void employeeCreation() {

        Employee expected = new Employee("Joao", "Moreira", "FT", "Dev", 5, "email@domain.com");

        Employee result = new Employee("Joao", "Moreira", "FT", "Dev", 5, "email@domain.com");

        assertEquals(expected, result);

    }

    @Test
    void employeeCreationNotEqual() {

        Employee expected = new Employee("Joao", "Moreira", "FT", "Dev", 5, "email@domain.com");

        Employee result = new Employee("Jose", "Moreira", "FT", "Dev", 5, "email@domain.com");

        assertNotEquals(expected, result);
    }

    @Test
    void TestConstructorForInvalidFirstName_Null() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Employee(null, "Moreira", "FT", "Dev", 5, "email@domain.com");
        });
    }

    @Test
    void TestConstructorForInvalidDescription_EmptyString() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Employee(null, "Moreira", "", "Dev", 5, "email@domain.com");
        });
    }

    @Test
    void validatesEmailForEmptyString() {

        //Arrange
        String emailTest = "";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = false;

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void validatesEmailForEmptyStringWithSpace() {

        //Arrange
        String emailTest = " ";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = false;

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void validatesEmailForDot() {

        //Arrange
        String emailTest = ".";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = false;

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void validatesEmailForAtSign() {

        //Arrange
        String emailTest = "@";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = false;

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void validatesEmailForLettersWithoutAtSign() {

        //Arrange
        String emailTest = "email";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = false;

        //Assert
        assertEquals(expected, result);

    }


    @Test
    void validatesEmail_CorrectEmailFormat() {

        //Arrange
        String emailTest = "user@domain.com";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = true;

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void validatesEmail_CorrectEmailFormat_2() {

        //Arrange
        String emailTest = "user@domain.com.pt";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = true;

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void validatesEmail_CorrectEmailFormat_3() {

        //Arrange
        String emailTest = "user.name@domain.com";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = true;

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void validatesEmail_CorrectEmailFormat_4() {

        //Arrange
        String emailTest = "user'name@domain.com.pt";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = true;

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void validatesEmail_CorrectEmailFormat_5() {

        //Arrange
        String emailTest = "user_name@domain.com";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = true;

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void validatesEmail_CorrectEmailFormat_6() {

        //Arrange
        String emailTest = "user-name@domaincom";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = true;

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void validatesEmail_IncorrectEmailFormat_1() {

        //Arrange
        String emailTest = "user#domain.com";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = false;

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void validatesEmail_IncorrectEmailFormat_2() {

        //Arrange
        String emailTest = "@yahoo.com";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = false;

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void validatesEmail_IncorrectEmailFormat_3() {

        //Arrange
        String emailTest = "user@user@yahoo.com";

        //Act
        boolean result = Employee.emailRegexPatternValidation(emailTest);
        boolean expected = false;

        //Assert
        assertEquals(expected, result);
    }

}