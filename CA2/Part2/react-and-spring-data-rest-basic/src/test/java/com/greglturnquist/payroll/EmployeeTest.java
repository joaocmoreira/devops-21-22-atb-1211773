package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.InputMismatchException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class EmployeeTest {

    @Test
    void employeeCreation() {

        Employee expected = new Employee("Joao", "Moreira", "FT", "Dev");

        Employee result = new Employee("Joao", "Moreira", "FT", "Dev");

        assertEquals(expected, result);

    }

    @Test
    void employeeCreationNotEqual() {

        Employee expected = new Employee("Joao", "Moreira", "FT", "Dev");

        Employee result = new Employee("Jose", "Moreira", "FT", "Dev");

        assertNotEquals(expected, result);
    }

    }

