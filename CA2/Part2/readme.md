# Class Assignment 2 Report

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered
in Bitbucket. See how to write README files for Bitbucket
in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the
folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

*A section dedicated to the description of the analysis, design and
implementation of the requirements*

*Should follow a "tutorial" style (i.e., it should be possible to
reproduce the assignment by following the instructions in the
tutorial)*

* *Should include a description of the steps used to achieve the
  requirements*
* *Should include justifications for the options (when required)*

## 1.1 **CA2-part1**

The first step was to download the example from https://bitbucket.org/luisnogueira/gradle_basic_demo/ and 
commit all the files to our own repository. 
The following commands were used to stage, commit and then push all the files:

> *$ git add .*
>
> *$ git commit -m "message"*
> 
> *$ git push origin master*



New task to run the server added to build.gradle class.

>*task runServer(type: JavaExec, dependsOn: classes) {*
> 
>*group = "DevOps"*
> 
>*description = "Launches a chat client that connects to a server on localhost:59001"*
> 
>*classpath = sourceSets.main.runtimeClasspath*
> 
>*mainClass = 'basic_demo.ChatServerApp'*
>
>*args '59001'*
> 
>*}*


A unit test was added and the gradle script updated. 
To run the unit test the Junit dependency was added.

>*dependencies {*
> 
> *testImplementation group: 'junit', name: 'junit', version: '4.12'*
> 
>*}*

To backup the sources of the application we used a task of type copy.
This task made a copy of the contents of the src folder to a new backup folder.com

>*task copyTask(type:Copy) {*
> 
>*from 'src'*
> 
>*into 'backup'*
> 
>*}*

A new task of type Zip was then added to copy de contents of src file to a zip file.com

>*task zip(type:Zip) {*
> 
>*from 'src'*
> 
>*into 'backup.zip'*
> 
>*destinationDir(file('backupZip'))*
> 
>*}*

## 1.2 **CA2-part2**

> *$ git branch email-field*
>
Command to create a new branch.

> *$ git branch*
>
>email-field
>
> *master
>
Command to show the available branches. The * before indicates the
active branch.

> *$ git checkout email-field*
>
Command to change the active branch.

The modifications to the project where applied as indicated on the
Statement.

> *$ git commit -a -m "first branch commit - email field added and tested - references #2"*
>
Command we will apply a commit with the message indicated.

> *$ git push origin email-field*
>
Command to push to the repository the open commits to the branch
email-field.

> *$ git tag -a v1.3.0 -m "my version 1.3.0"*
>
Creation of the new TAG and the respective message.

> *$ git checkout master*
>
Change the active branch to master.

> *$ git merge email-field*
>
Command to merge the branch email-field. Modifications made on the
branch email-field will be applied on the branch master.

> *$ git push origin v1.3.0*
>
Apply the tag on the last push.

> *$ git branch fix-invalid-email*
>
Create a new branch

> *$ git checkout fix-invalid-email*
>
Change the active branch to fix-invalid-email.

Modifications to the project applied as indicated on the Statement.

> *$ git commit -a -m "Email validation added - references #2"*
>
Commit the changes and also reference the commit to the issue number
#2.

> *$ git commit -a -m "Added test to verify the email exception - references #2"*
>
Commit changes, new tests.


> *$ git checkout master*
>
>*$ git merge fix-invalid-email*
>
Change active branch and merge the changes.

> *$ git push  origin master*
>
Push the commits.

> *$ git tag -a CA1-part2 -m "CA1-part2 closed"*
>
Create new tags.

> *$ git push  origin CA1-part2*
>
Apply the tag to the last commit.

## 2. Analysis of an Alternative

The alternative chosen to compare was Mercurial for the version
control and Helix Team Hub for the repository.

### 2.1 Git vs Mercurial





## 3. Implementation of the Alternative

### 3.1 Install Mercurial

My OS is Linux UBUNTU, so it was used the terminal to install:

> *sudo apt-get install mercurial

### 3.2 Registration on the HELIX TEAM HUB

The registration was made online and a repository with the name
devops-21-22-atb-1040817 was created.

### 3.3 Steps

To the implementation, instead of using the React & Spring
application, it was simulated the steps using simple Markdown files.

* CA1-part1 alternative

Create a clone from the repo to the local machine:

> *$ hg clone https://JSoares@helixteamhub.cloud/wild-jar-9431/projects/devops-21-22-atb-1040817/repositories/mercurial/devops-21-22-atb-1040817*
>
The file CA1-part1-1stFile.md was added to the directory. To track the
new file it was used the command below.

> *$ hg add*
>

Commit the new file to the repo.

> *$ hg commit -m "First Commit"*
>
Push the commit to the repository
> *$ hg push*

Create a new TAG.
> *$ hg tag v1.1.0*
>
Push the TAG to the last commit
> *$ hg push*

The first file was modified and created a new one. Track the new file,
commit and push the commit.
> *$ hg add*
>

> *$ hg push*
>
and the last TAG Create a new TAG.
> *$ hg tag ca1-part1*
>
Push the TAG to the last commit
> *$ hg push*
>

* CA1-part2 alternative

Create the branch email-field.
> *$ hg branch email-field*
>
Made some modifications on the 1st commit file and commit and push the
changes.
> *$ hg commit -m "first commit on the branch"*
>
>*$ hg push*
>
Activate the default branch, merge the changes commit and push.
> *$ hg update default*
>
> *$ hg merge email-field*
>
> *$ hg commit -m "Commit after merge"*
>
> *$ hg push*



Create a new branch, modify the files and merge the branches
> *$ hg branch fix-invalid-email*
>
>*$ hg commit -m "first commit on the branch fix-email-field"*
>
>*$ hg push*
>
>*$ hg update default*
>
>*$ hg merge fix-invalid-email*

Create a new TAG.
> *$ hg tag v1.3.1*
>
Push the TAG to the last commit
> *$ hg push*
>
Create a new TAG.
> *$ hg tag CA1-part2*
>
Push the TAG to the last commit
> *$ hg push*
>
Below the image of the last commits on the Helix Team Hub Repo

![PrintScreenLastCommit](PrintScreenLastCommit.png)




