# Class Assignment 3 Part 1

## 1. Analysis, Design and Implementation

To perform the requested tasks, it is necessary to install ubuntu 18.04 in a
virtual machine using virtualBox

###### Other software used to performe this class assignment:
**- Local machine:**</p>
  - Visual studio code (linked to the ubuntu into the VM via ssh for editing
    files)</p>
  - Filezilla (STP/SFTP client to access the ubuntu installed in the VM and manage
    directories visually)</p>
**- VirtualBox ubuntu:**</p>
  - Openssh to connect to enable the connection via ssh for filezilla, visual
    studio code and window terminals.</p>
  - Git (sudo apt install git)</p>

#### Tasks:

**Executing tut-react-and-spring-data-rest (CA1)**

For executing the first tutorial it was used the followed command:
Command Line

```bash 
# To build the application 
./mvnw build 
# To run build the application
./mvnw spring-boot:run 
```

To display the fronted of the application I used to go to the VM address
  192.168.56.5:8080.</p>


**Executing gradle_basic_demo (CA2)**

For executing the first tutorial it was used the followed commands:

```bash  
# Build the project:
./gradlew build
```

```bash  
# Run the app server:
./gradlew runServer
```

For getting the results for the app chat, it was necessary open it in the
  local IDEA and run the following command:

```bash  
# Run app client
./gradlew runClient
```
**It is necessary to update the server address on the task runClientApp:**
```groovy  
// From:
    args 'localhost', '59001'
// To:
    args '192.168.56.5', '59001'
```


