# CA3 - Virtual Machines - part 2


## Part 2 

# Create a Vagrantfile to work with Virtualbox provider


 
### Create Vagrantfile

The vagrant file used was a copy of the example given.

Some changes were made to use my own repository, but after several tries even with the build being successful the web was just swoyng a blank page. I decided to use the example application given, to demonstrate the use of vagrant and the creation of the virtual box.

https://bitbucket.org/joaocmoreira/devops-21-22-atb-1211773/src/master/CA3/Part2/vagrant-virtualbox/Vagrantfile


### Start installation of the virtual machine
Virtualbox is the default provider no need to specify
```bash
vagrant up --provision
```
<br>



### Open virtual machine in ssh terminal
No password needed at this level
<sub><sup><em>Default username and password : vagrant</em></sub></sup>
```bash
vagrant ssh web
```

### Open web machine
hostnames : localhost | 192.168.56.10 <br>
port : 8080

hhttp://localhost:8080/basic-0.0.1-SNAPSHOT/

http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/

### Open db machine
hostnames : localhost | 192.168.56.11 <br>
port : 8081

http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console/

<br>

---

